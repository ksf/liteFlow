/**
 * <p>Title: liteFlow</p>
 * <p>Description: 轻量级的组件式流程框架</p>
 * <p>Copyright: Copyright (c) 2017</p>
 * @author Bryan.Zhang
 * @email weenyc31@163.com
 * @Date 2017-7-28
 * @version 1.0
 */
package com.thebeastshop.liteflow.entity.config;

import com.thebeastshop.liteflow.core.Component;

public class Node {
	
	private String id;
	
	private String clazz;
	
	private Component instance;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getClazz() {
		return clazz;
	}

	public void setClazz(String clazz) {
		this.clazz = clazz;
	}

	public Component getInstance() {
		return instance;
	}

	public void setInstance(Component instance) {
		this.instance = instance;
	}
}
